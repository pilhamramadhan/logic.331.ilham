package DeretAngka;

public class Soal02 {
    public static void Hasil (int n){
        int nilai = 2;
        int[] hasil = new int[n];
        for (int i = 0; i < n; i++) {
            hasil[i] = nilai;
            nilai += 2;
        }
        Utility.Print(hasil);
    }
}
