package DeretAngka;

public class Soal12 {
    public static void Hasil(int n){

        int helper = 1;
        boolean flag = true;

        int[] hasil = new int[n];

        for (int i = 0; i < n; i++) {
            while (flag){
                if (helper == 1){
                    helper++;
                }
                else if (helper == 2) {
                    hasil[i] = helper;
                    helper++;
                    flag = false;
                } else if (helper == 3) {
                    hasil[i] = helper;
                    helper++;
                    flag = false;
                } else if (helper % 2 != 0 && helper % 3 !=0) {
                    hasil[i] = helper;
                    helper++;
                    flag = false;
                } else {
                    helper++;
                }
            }
            flag = true;
        }
        Utility.PrintBo(hasil);
    }
}
