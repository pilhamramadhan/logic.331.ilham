package DeretAngka;

public class Soal04 {
    public static void Hasil (int n){
        int nilai = 1;
        int[] hasil = new int[n];
        for (int i = 0; i < n; i++) {
            hasil[i] = nilai;
            nilai += 4;
        }
        Utility.Print(hasil);
    }
}
