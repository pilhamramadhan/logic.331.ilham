package DeretAngka;

public class Soal01 {
    public static void Hasil(int n){

        int nilai = 1;
        int[] hasil = new int[n];

        for (int i = 0; i < n ; i++) {
            hasil[i] = nilai;
            nilai += 2;
        }
        Utility.Print(hasil);
    }
}
