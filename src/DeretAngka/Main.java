package DeretAngka;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        boolean flag = true;
        int pilihan = 0 ;
        int n = 0 ;

        while (flag){
            System.out.println("soal nomer 1 - 12");
            System.out.print("nomor = ");
            pilihan = input.nextInt();

            while (pilihan < 1 || pilihan > 12){
                System.out.println("angka tidak tersedia");
                pilihan=input.nextInt();
            }

            System.out.print("masukan n = ");
            n = input.nextInt();

            switch (pilihan){
                case 1 :
                    Soal01.Hasil(n);
                    break;
                case 2:
                    Soal02.Hasil(n);
                    break;
                case 3:
                    Soal03.Hasil(n);
                    break;
                case 4:
                    Soal04.Hasil(n);
                    break;
                case 5:
                    Soal05.Hasil(n);
                    break;
                case 6:
                    Soal06.Hasil(n);
                    break;
                case 7:
                    Soal07.Hasil(n);
                    break;
                case 8:
                    Soal08.Hasil(n);
                    break;
                case 9:
                    Soal09.Hasil(n);
                    break;
                case 10:
                    Soal10.Hasil(n);
                    break;
                case 11:
                    Soal11.Hasil(n);
                    break;
                case 12:
                    Soal12.Hasil(n); // belum ketemu
                    break;           // jujur nya belum dicoba

            }

        }

    }
}