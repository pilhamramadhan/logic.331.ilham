package DeretAngka;

public class Soal03 {
    public static void Hasil (int n){
        int nilai = 1;
        int[] hasil = new int[n];
        for (int i = 0; i < n; i++) {
            hasil[i] = nilai;
            nilai += 3;
        }
        Utility.Print(hasil);
    }
}
