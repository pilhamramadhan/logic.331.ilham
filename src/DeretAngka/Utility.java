package DeretAngka;

public class Utility {
    public static void Print(int[] hasil){
        for (int i = 0; i < hasil.length ; i++) {
            if (hasil[i] == 0){
                System.out.print("* ");
            } else {
                System.out.print(hasil[i]+ " ");
            }
        }
        System.out.println(" ");
        System.out.println(" ");
    }

    public static void PrintBintang(int[] hasil){
        for (int i = 0; i < hasil.length ; i++) {
            if (hasil[i] % 3 == 0){
                System.out.print("* ");
            }else {
                System.out.print(hasil[i]+ " ");
            }
        }
        System.out.println(" ");
        System.out.println(" ");
    }

    public static void PrintX(int[] hasil){
        int angka = 1;
        for (int i = 0; i < hasil.length; i++) {
            if ( angka % 4 == 0){
                System.out.print("XXX ");
                angka++;
            }else {
                System.out.print(hasil[i] + " ");
                angka++;
            }
        }
        System.out.println(" ");
        System.out.println(" ");
    }

    public static void PrintBo(int[] hasil){
        for (int i = 0; i < hasil.length; i++) {
            System.out.print(hasil[i] + " ");
        }
        System.out.println(" ");
        System.out.println(" ");
    }

}
