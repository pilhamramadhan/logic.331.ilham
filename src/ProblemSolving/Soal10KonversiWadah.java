package ProblemSolving;

import java.util.Scanner;

public class Soal10KonversiWadah {
    public static void Hasil() {
        Scanner input = new Scanner(System.in);

        System.out.println("ukuran tiap wadah");
        System.out.println("1 botol = 2 gelas, 1 teko = 25 cangkir, 1 gelas = 2.5 cangkir");

        System.out.println("masukan nama wadah = ");
        String inputwadah = input.nextLine();
        while (!inputwadah.equals("botol") && !inputwadah.equals("cangkir") && !inputwadah.equals("teko") && !inputwadah.equals("gelas")) {
            System.out.println("tidak ada pilihan");

            System.out.println("masukan nama wadah = ");
            inputwadah = input.nextLine();
        }

        System.out.println("masukan berapa " + inputwadah + " = ");
        double banyakUkuran = input.nextInt();

        System.out.println("di konversi ke wadah apa =");
        input.nextLine();
        String konversiWadah = input.nextLine();
        while (!konversiWadah.equals("botol") && !konversiWadah.equals("cangkir") && !konversiWadah.equals("teko") && !konversiWadah.equals("gelas")) {
            System.out.println("tidak ada pilihan");

            System.out.println("di konversi ke wadah apa =");
            konversiWadah = input.nextLine();
        }

        if (inputwadah.equals("botol")) {
            if (konversiWadah.equals("cangkir")) {
                banyakUkuran *= 5;
            } else if (konversiWadah.equals("gelas")) {
                banyakUkuran *= 2;
            } else if (konversiWadah.equals("teko")) {
                banyakUkuran /= 5;
            } else if (konversiWadah.equals("botol")) {
                banyakUkuran *= 1;
            }
            System.out.println("hasil = " + banyakUkuran + " " + konversiWadah);
        }

        if (inputwadah.equals("gelas")) {
            if (konversiWadah.equals("botol")) {
                banyakUkuran /= 2;
            } else if (konversiWadah.equals("cangkir")) {
                banyakUkuran *= 2.5;
            } else if (konversiWadah.equals("teko")) {
                banyakUkuran /= 10;
            } else if (konversiWadah.equals("gelas")) {
                banyakUkuran *= 1;
            }
            System.out.println("hasil = " + banyakUkuran + " " + konversiWadah);
        }

        if (inputwadah.equals("teko")) {
            if (konversiWadah.equals("cangkir")) {
                banyakUkuran *= 25;
            } else if (konversiWadah.equals("gelas")) {
                banyakUkuran *= 10;
            } else if (konversiWadah.equals("botol")) {
                banyakUkuran *= 5;
            } else if (konversiWadah.equals("teko")) {
                banyakUkuran *= 1;
            }
            System.out.println("hasil = " + banyakUkuran + " " + konversiWadah);
        }

        if (inputwadah.equals("cangkir")) {
            if (konversiWadah.equals("cangkir")) {
                banyakUkuran *= 1;
            } else if (konversiWadah.equals("gelas")) {
                banyakUkuran /= 4;
            } else if (konversiWadah.equals("botol")) {
                banyakUkuran /= 5;
            } else if (konversiWadah.equals("teko")) {
                banyakUkuran /= 25;
            }
            System.out.println("hasil = " + banyakUkuran + " " + konversiWadah);
        }
    }
}
