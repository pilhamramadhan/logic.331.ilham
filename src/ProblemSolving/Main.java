package ProblemSolving;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0 ;
        boolean flag = true;

        while (flag){
            System.out.println("soal nomer 1 - 14");
            System.out.print("nomor = ");
            pilihan = input.nextInt();

            while (pilihan < 1 || pilihan > 14){
                System.out.println("nomor tidak tersedia");
                pilihan = input.nextInt();
            }


            switch (pilihan){
                case 1 :
                    Soal01.Hasil();
                    break;
                case 2:
                    Soal02JarakToko.Hasil();
                    break;
                case 3:
                    Soal03NambahjumlahBuah.Hasil();
                    break;
                case 4:
                    Soal04BeliBarang.Hasil();
                    break;
                case 5:
                    Soal05GantiJam.Hasil();
                    break;
//                case 6:
//                    Soal6.HasilArray();
//                    break;
                case 7:
                    Soal7jimmyJalan.Hasil();
                    break;
                case 8:
                    Soal08KacaMata.Hasil();
                    break;
                case 9:
                    Soal09Parkir.Hasil();
                    break;
                case 10:
                    Soal10KonversiWadah.Hasil();
                    break;
                case 11:
                    Soal11PointPulsa.Hasil();
                    break;
                case 12:
                    Soal12NambahJumlahBuah.Hasil();
                    break;
                case 13:
                    Soal13SanakanAngkadanHuruf.Hasil();
                    break;

            }

        }

    }
}