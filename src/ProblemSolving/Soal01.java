package ProblemSolving;

import java.util.Scanner;

public class Soal01 {
    public static void Hasil(){
        Scanner input = new Scanner(System.in);

        System.out.print("masukan panjang deret = ");
        int masukan = input.nextInt();

        int[] deret1 = new int[masukan];
        int[] deret2 = new int[masukan];
        int[] jumlahDeret = new int[masukan];

        System.out.print("deret pertama = ");
        for (int i = 0; i < deret1.length; i++) {
            deret1[i] = i * 3 - 1 ;
            System.out.print(deret1[i] + " ");
        }

        System.out.println(" ");
        System.out.print("deret kedua = ");
        for (int i = 0; i < deret2.length; i++) {
            deret2[i] = ((-2) * i);
            System.out.print(deret2[i] + " ");
        }

        System.out.println(" ");
        System.out.print("hasil penjumlahan = ");
        for (int i = 0; i < jumlahDeret.length; i++) {
            jumlahDeret[i] = deret1[i] + deret2[i];
            System.out.print(jumlahDeret[i]+ " ");
        }
        System.out.println(" ");
    }
}
