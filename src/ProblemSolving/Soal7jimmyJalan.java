package ProblemSolving;

import java.util.Scanner;

public class Soal7jimmyJalan {
    public static void Hasil(){
        Scanner input = new Scanner(System.in);


        System.out.println("Masukkan Pola Lintasan : ");
        String jalan = input.nextLine();


        System.out.println("Masukkan Pilihan Cara Jalan : ");
        String CaraJalan = input.nextLine().toLowerCase();

        int energi = 0;
        boolean jatuh = true;
        int helper = 0;

        for (int i = 0; i < CaraJalan.length(); i++) {
            if (CaraJalan.charAt(i) == 'w' && jalan.charAt(helper) == '-')
            {
                energi++;
                helper++;
            }
            else if (CaraJalan.charAt(i) == 'w' && jalan.charAt(helper) == 'o')
            {
                jatuh = false;
                break;
            }
            else if (CaraJalan.charAt(i) == 'j' && energi >= 2 )
            {
                helper += 2;
                if(jalan.charAt(helper - 1) == 'o')
                {
                    jatuh = false;
                    break;
                }
                else
                {
                    energi -= 2;
                }
            }
            else if (CaraJalan.charAt(i) == 'j' && jalan.charAt(helper) == 'o' && energi < 2)
            {
                jatuh = false;
                break;
            }
        }

        if (!jatuh)
        {
            System.out.println("Jim Died");
        }

        if (jatuh)
        {
            System.out.print("hasil energi = "+energi);
        }

        System.out.println();
    }
}
