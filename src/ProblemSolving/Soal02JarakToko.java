package ProblemSolving;

import java.util.Scanner;

public class Soal02JarakToko {
    public static void Hasil(){
        Scanner input = new Scanner(System.in);

        System.out.println("jarak antar tokoh");
        System.out.println("toko 1 = 0.5 Km");
        System.out.println("toko 2 = 2 Km");
        System.out.println("toko 3 = 3.5 Km");
        System.out.println("toko 4 = 5 Km");

        System.out.print("masukan berapa toko yang ingin di inputkan = ");
        String masukan = input.nextLine();
        String jarakToko = "0 0.5 2 3.5 5";
        double jarakTempuh = 0;
        double selisihJarak = 0;
        double selisihTempuh = 0;
        int waktu;


        int[] toko = Utility.ConvertStringToArray(masukan);
        double[] tempuh = Utility.ConvertStringToArraydouble(jarakToko);

        for (int i = 0; i < toko.length; i++) {
            if (i == 0){
                jarakTempuh = tempuh[toko[i]];
            } else if (i == toko.length - 1) {
                selisihJarak = tempuh[toko[i]] - tempuh[toko[i - 1]];
                if (selisihJarak < 0){
                    selisihJarak *= -1;
                }
                jarakTempuh += (selisihJarak + tempuh[toko[i]]);
            } else {
                selisihTempuh = tempuh[toko[i]] - tempuh[toko[i - 1]];
                if (selisihTempuh < 0){
                    selisihTempuh *= -1;
                }
                jarakTempuh += selisihTempuh;
            }
        }
        System.out.println("Jarak yang ditempuh = " + jarakTempuh + " KM");

        waktu = (int) ((jarakTempuh * 2) + (toko.length * 10));
        System.out.println("Waktu yang diperlukan = " + waktu + " Menit" );
    }
}
