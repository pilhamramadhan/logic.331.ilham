package ProblemSolving;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Soal09Parkir {
    public static void Hasil(){
        Scanner input = new Scanner(System.in);

        DateFormat jam = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");

        System.out.println("jam masuk parkir = ");
        String jamMasuk = input.nextLine();

        System.out.println("jam keluar = ");
        String jamKeluar = input.nextLine();

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = jam.parse(jamMasuk);
        } catch (ParseException e){
            throw new RuntimeException(e);
        }
        try {
            d2 = jam.parse(jamKeluar);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        long lamaWaktu = d2.getTime() - d1.getTime();
        long lamaWaktuJam = lamaWaktu / (1000 * 60 * 60);

        System.out.println(lamaWaktuJam + " jam");

        int hargaParkir = 0;
        int hari = 0;

        if(lamaWaktuJam <= 8)
        {
            hargaParkir = (int) lamaWaktuJam * 1000;
        } else if (lamaWaktuJam > 8 && lamaWaktuJam <= 24) {
            hargaParkir = 8000;
        }
        else
        {
            hari = (int) lamaWaktuJam / 24;
            hargaParkir = hari * 15000;
        }
        System.out.println(hargaParkir);
    }
}
