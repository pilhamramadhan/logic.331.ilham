package ProblemSolving;

import java.util.HashMap;
import java.util.Scanner;

public class Soal03NambahjumlahBuah {
    public static void Hasil(){
        Scanner input = new Scanner(System.in);
        HashMap<String, Integer> mapPing = new HashMap<String, Integer>();

        System.out.println("masukan data buah = ");
        String data = input.nextLine();

        String[] dataSplit = data.split(", ");
        String[] item = new String[2];
        int helper = 0;

        for (int i = 0; i < dataSplit.length; i++) {
            item = dataSplit[i].split(":");
            if (mapPing.get(item[0]) == null  ){
                mapPing.put(item[0], Integer.parseInt(item[1]));
            }
            else {
                helper = mapPing.get(item[0]);
                mapPing.put(item[0], Integer.parseInt(item[1]) + helper);
            }
        }

        for (String key : mapPing.keySet()){
            System.out.println(key + " " + mapPing.get(key));
        }
    }
}
