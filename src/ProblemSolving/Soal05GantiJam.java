package ProblemSolving;

import java.util.Scanner;

public class Soal05GantiJam {
    public static void Hasil(){
        Scanner input = new Scanner(System.in);

        System.out.println("masukan jam = ");
        String jam = input.nextLine();

        String jamSub = jam.substring(0,2);
        int jamInt = Integer.parseInt(jamSub);
        String konversi =Integer.toString(jamInt);


        if (jam.contains("am")){
            if (jamInt >= 12){
                jamInt -= 12;
                jam = jam.replace(jamSub, konversi);
                jam = jam.replace("am", "");
                System.out.println(jamInt+ ":"+ jam.substring(3,5) +"pm");
            } else if (jamInt < 12) {
                jam = jam.replace("am","");
                System.out.println(jam + " am");
            }
        }
        else if (jam.contains("pm")) {
            if (jamInt <= 12){
                jamInt += 12;
                jam = jam.replace(jamSub, konversi);
                jam = jam.replace("pm","");
                System.out.println(jamInt+ ":"+ jam.substring(3,5) + " am");
            } else if (jamInt > 12) {
                jam = jam.replace("pm","");
                System.out.println(jam +" pm");
            }
        }
        else if (jamInt < 24 && jamInt > 12) {
            jamInt -= 12;
            jam = jam.replace(jamSub, konversi);
            System.out.println(jamInt+ ":"+ jam.substring(3,5) + " pm");
        }
        else if (jamInt < 12) {
            jam = jam.replace(jamSub, konversi);
            System.out.println(jam + " am");
        }
    }
}
