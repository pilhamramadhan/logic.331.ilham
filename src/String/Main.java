package String;



import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        boolean flag = true;


//        System.out.println("soal nomer 1 - 12");
        System.out.print("nomor = ");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 10) {
            System.out.println("soal tidak tersedia");
            pilihan = input.nextInt();
        }


        switch (pilihan) {
            case 1:
                CamelCase.Hasil();
                break;
            case 2:
                StrongPassword.Hasil();
                break;
            case 3:
                CaesarCipher.Hasil();
                break;
            case 4:
                MarsExploration.Hasil();
                break;
            case 5:
                HackerRank.Hasil();
                break;
            case 6:
                Pangrams.Hasil();
                break;
            case 8:
                gemsstone.Hasil();
                break;
            case 9:
                MakingAnagram.Hasil();
                break;
            case 10:
                TwoString.Hasil();

        }
    }
}