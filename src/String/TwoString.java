package String;

import java.util.Scanner;

public class TwoString {
    public static void Hasil(){
        Scanner input = new Scanner(System.in);

        System.out.print("masukan kalimat 1 = ");
        String kalimat1 = input.nextLine();
        System.out.print("masukan kalimat 2 = ");
        String kalimat2 = input.nextLine();

        char[] kalimat1Char = kalimat1.toCharArray();
        char[] kalimat2Char = kalimat2.toCharArray();

        int nilai = 0;
        for (int i = 0; i < kalimat1Char.length; i++) {
            for (int j = 0; j < kalimat2Char.length; j++) {
                if (kalimat1Char[i] == kalimat2Char[i]){
                    nilai++;
                }
            }
        }

        if (nilai != 0){
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
    }
}

