package String;

import java.util.Scanner;

public class MakingAnagram {
    public static void Hasil(){
        Scanner input = new Scanner(System.in);
        int count = 0;

        System.out.print("Masukan kalimat 1: ");
        String kalimat1 = input.nextLine();

        System.out.print("Masukan kalimat 2: ");
        String kalimat2 = input.nextLine();

        char[] s1Char = kalimat1.toCharArray();
        char[] s2Char = kalimat2.toCharArray();

        for (int i = 0; i < 2; i++) {
            if (i == 0) {
                for (int j = 0; j < s1Char.length; j++) {
                    if (s1Char[j] != 'c') {
                        count ++;
                    }
                }
            } else if (i == 1) {
                for (int j = 0; j < s2Char.length; j++) {
                    if (s2Char[j] != 'c') {
                        count ++;
                    }
                }
            }
        }
        System.out.println(count);
    }
}
