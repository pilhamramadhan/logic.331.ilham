package finalpratice;

import java.util.Scanner;

public class SoalNomer4ikan {
    public static void Hasil(){
        Scanner input = new Scanner(System.in);
        System.out.println("Contoh input : 42, 50, 30, 70");
        System.out.println("Masukkan Data Harga dalam Ribuan Rupiah : ");
        String data = input.nextLine();
        System.out.println("Masukkan kurs dollar sekarang : ");
        int kurs = input.nextInt();
        String[] dataString = data.split(", ");
        double[] dataArrayDou = new double[dataString.length];
        double temenGabisaIkan = 0;
        double temenLain = 0;

        for (int i = 0; i < dataString.length; i++) {
            dataArrayDou[i] = Double.parseDouble(dataString[i]);
            dataArrayDou[i] = dataArrayDou[i] * 115 / 100;
            dataArrayDou[i] = dataArrayDou[i] / kurs;
        }

        for (int i = 0; i < dataArrayDou.length; i++) {
            if (i == 0)
            {
                temenLain = temenLain + (dataArrayDou[i] / 3);
            }
            else
            {
                temenLain = temenLain + (dataArrayDou[i] / 4);
                temenGabisaIkan = temenGabisaIkan + (dataArrayDou[i] / 4);
            }
        }

        System.out.println("Temen yang Ga Bisa Makan Ikan, Bayar = " + temenGabisaIkan);
        System.out.println("Yang Bisa Makan Ikan Bayar = " + temenLain);

    }
}
