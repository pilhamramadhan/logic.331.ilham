package finalpratice;

import java.util.Scanner;

public class SoalNomer3Rotasi {
    public static void Hasil(){
        Scanner input = new Scanner(System.in);

        System.out.println("masukan deret angka = ");
        String masukan = input.nextLine();
        String[] masukanArray = masukan.split(" ");
        int[] numbers = new int[masukanArray.length];
        for (int i = 0; i < masukanArray.length; i++) {
            numbers[i] = Integer.parseInt(masukanArray[i]);
        }
        int rotasi, helper;

        System.out.println("Deret angka");
        for (int number : numbers) {
            System.out.print(number + " ");
        }
        System.out.println();
        System.out.print("Anda ingin melakukan berapa kali rotasi: ");
        rotasi = input.nextInt();


        for (int i = 0; i < rotasi; i++) {
            helper = numbers[0];
            for (int j = 0; j < numbers.length; j++) {
                if (j == numbers.length - 1){
                    numbers[j] = helper;
                }
                else {
                    numbers[j] = numbers[j + 1];
                }
            }
        }

        System.out.print("Hasil Rotasi: ");
            for (int number: numbers) {
                System.out.println(number + " ");
            }

        System.out.println();
    }
}
