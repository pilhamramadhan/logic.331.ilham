package finalpratice;

import java.util.Scanner;

public class SoalNomer10ResepKue {
    public static void Hasil(){
        Scanner input = new Scanner(System.in);

        System.out.println("resep kue dari banyak nya n = ");
        int n = input.nextInt();

        double tepung = 125.0 / 15.0;
        double gula = 100.0 / 15.0;
        double susu = 100.0 / 15.0;

        double jumlahTepung = n*tepung;
        double jumlahGula = n*gula;
        double jumlahSusu = n*susu;

        System.out.println("untuk buat " + n + " cupcake butuh");
        System.out.println(jumlahGula + "gr gula");
        System.out.println(jumlahSusu + "ml susu");
        System.out.println(jumlahTepung + "gr tepung");

    }
}
