package finalpratice;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        boolean flag = true;


//        System.out.println("soal nomer 1 - 12");
        System.out.print("nomor = ");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 12) {
            System.out.println("soal tidak tersedia");
            pilihan = input.nextInt();
        }


        switch (pilihan) {
            case 1:
                SoalNomer1Bambangloli.Hasil();
                break;
            case 2:
                SoalNomer2dibolakbalikSama.Hasil();
                break;
            case 3:
                SoalNomer3Rotasi.Hasil();
                break;
            case 4:
                SoalNomer4ikan.Hasil();
                break;
            case 5:
                SoalNomer5konverijam.Hasil();
                break;
            case 6:
                SoalNomer6perpus.Hasil();
                break;
            case 7:
                SoalNomer7MeanMedianModus.Hasil();
                break;
            case 9:
                SoalNomer9sudutjam.Hasil();
                break;
            case 10:
                SoalNomer10ResepKue.Hasil();
                break;

        }
    }
}
