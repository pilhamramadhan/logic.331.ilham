import java.util.Arrays;
import java.util.Scanner;

public class FT1_ilham_soal3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("masukan kalimat =");
        String kalimat = input.nextLine();
        System.out.println("di penggal berapa = ");
        int penggal = input.nextInt();

        char[] hurufPisah = kalimat.toLowerCase().toCharArray();
        char helper;

        for (int i = 0; i < hurufPisah.length - 1 ; i++) {
            for (int j = i + 1; j < hurufPisah.length; j++) {
                if (hurufPisah[i] > hurufPisah[j]){
                    helper = hurufPisah[i];
                    hurufPisah[i] = hurufPisah[j];
                    hurufPisah[j] = helper;
                }
            }
        }

            System.out.println(hurufPisah);

    }
}
