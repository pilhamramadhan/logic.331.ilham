package BangunDatar;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
//       String nama  = "ilham ramadha p";
//       String asal = "kabupaten jember";
//       int umur = 23;
//       boolean flag = true;
//
//        System.out.println("nama saya " + nama);
//        System.out.println("saya tinggal di "+ asal +" dan umur saya adalah " + umur);

        Scanner input = new Scanner(System.in);

        boolean flag = true;
        String answer = "y";

        while (flag) {
            System.out.println("silahakan pilih ?");
            System.out.println("1. persegi panjang");
            System.out.println("2. segitiga");
            System.out.println("3. trapesium");
            System.out.println("4. lingkaran");
            System.out.println("5. persegi");

            int pilihan1 = input.nextInt();
            int pilihan2;

            String prompt = "pilih   1. luas     2. keliling";


            switch (pilihan1) {
                case 1:
                    System.out.println(prompt);
                    pilihan2 = input.nextInt();

                    if (pilihan2 == 1) {
                        PersegiPanjang.Luas();
                    } else if (pilihan2 == 2) {
                        PersegiPanjang.Keliling();
                    }
                    break;

                case 2:
                    System.out.println(prompt);
                    pilihan2 = input.nextInt();

                    if (pilihan2 == 1) {
                        Segitiga.LuasSegitiga();
                    } else if (pilihan2 == 2) {
                        Segitiga.KelilingSegitiga();
                    }
                    break;
                case 3:
                    System.out.println(prompt);
                    pilihan2 = input.nextInt();

                    if (pilihan2 == 1) {
                        Trapesium.LuasTrapesium();
                    } else if (pilihan2 == 2) {
                        Trapesium.KelilingTrapesium();
                    }
                    break;
                case 4:
                    System.out.println(prompt);
                    pilihan2 = input.nextInt();
                    if (pilihan2 == 1) {
                        Lingkaran.LuasLingkaran();
                    } else if (pilihan2 == 2) {
                        Lingkaran.KelilingLingkaran();
                    }
                    break;
                case 5:
                    System.out.println(prompt);
                    pilihan2 = input.nextInt();
                    if (pilihan2 == 1) {
                        Persegi.LuasPersegi();
                    } else if (pilihan2 == 2) {
                        Persegi.KelilingPersegi();
                    }
                    break;
                default:
                    System.out.println("tidak ada di pilihan");
            }


            System.out.println("mau coba lagi?  y/n");
            input.nextLine();
            answer = input.nextLine();

            if (!answer.toLowerCase().equals("y")) {
                flag = false;
            }
        }
    }
}