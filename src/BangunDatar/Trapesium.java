package BangunDatar;

import java.util.Scanner;

public class Trapesium {

    private static Scanner input = new Scanner(System.in);
    private static double alasAtas;
    private static double alasBawah ;
    private static double miringKanan ;
    private static double miringKiri ;
    private static double tinggiTrapesium;

    public static void LuasTrapesium(){
        System.out.print("masukan sisi atas trapesium = ");
            alasAtas = input.nextDouble();
            System.out.print("masukan nilai alas trapesium = ");
            alasBawah  = input.nextDouble();
            System.out.print("masukan nilai sisi kanan trapesium = ");
            miringKanan = input.nextDouble();
            System.out.print("masukan nilai sisi kiri trapesium = ");
            miringKiri = input.nextDouble();
            System.out.print("masukan nilai tinggi trapesium = ");
            tinggiTrapesium = input.nextDouble();

            double luasTrapesium = 0.5 * ((alasAtas + alasBawah) * tinggiTrapesium);
            System.out.println("hasil luas trapesium = " + luasTrapesium);

    }

    public static void KelilingTrapesium(){
        alasAtas = input.nextDouble();
        System.out.print("masukan nilai alas trapesium = ");
         alasBawah  = input.nextDouble();
        System.out.print("masukan nilai sisi kanan trapesium = ");
         miringKanan = input.nextDouble();
        System.out.print("masukan nilai sisi kiri trapesium = ");
        miringKiri = input.nextDouble();
        System.out.print("masukan nilai tinggi trapesium = ");
         tinggiTrapesium = input.nextDouble();


        double kelilingTrapesium = alasAtas + alasBawah + miringKanan + miringKiri;
        System.out.println("hasil keliling trapesium = " + kelilingTrapesium);
    }
}
// luas dan keliling trapesium
//