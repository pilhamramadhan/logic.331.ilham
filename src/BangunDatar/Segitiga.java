package BangunDatar;

import java.util.Scanner;

public class Segitiga {

    private static Scanner input = new Scanner(System.in);
    private static int alas;
    private static int tinggi;

    public static void LuasSegitiga() {
        // luas dan keliling segitiga
            System.out.print("masukan alas segitiga = ");
            alas = input.nextInt();
            System.out.print("masukan tinggi segitiga = ");
            tinggi = input.nextInt();

            int luas = (alas * tinggi) / 2;
            System.out.println("ini hasil luas segitiga = " + luas);

    }
    public static void KelilingSegitiga (){
        System.out.print("masukan alas segitiga = ");
        alas = input.nextInt();
        System.out.print("masukan tinggi segitiga = ");
        tinggi = input.nextInt();

        int keliling = alas * 3 ;
        System.out.println("ini hasil keliling segitiga = " + keliling);

    }
}
