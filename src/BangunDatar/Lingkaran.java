package BangunDatar;

import java.util.Scanner;

public class Lingkaran {
    private static Scanner input = new Scanner(System.in);
    private static double jariJari;
    private static double pi;


    public static void LuasLingkaran(){
        // luas dan keliling lingkaran
            System.out.print("masukan nilai jari jari lingkaran = ");
             jariJari = input.nextDouble();
            pi = 22/7;

            double luasLingkaran = pi * Math.pow(jariJari, 2);
            System.out.println("luas lingkaran = " + luasLingkaran);

    }
    public static void KelilingLingkaran(){
        // luas dan keliling lingkaran
            System.out.print("masukan nilai jari jari lingkaran = ");
            jariJari = input.nextDouble();
            pi = 22/7;


            double kelilingLingkaran = 2 * pi * jariJari;
            System.out.println("keliling lingkaran = " + kelilingLingkaran);
    }
}
