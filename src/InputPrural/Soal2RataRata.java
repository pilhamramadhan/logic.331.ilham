package InputPrural;

import java.util.Scanner;

public class Soal2RataRata {
    public static void resolve(){
        Scanner input = new Scanner(System.in);

        System.out.print("input deret angka = ");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;
        int nilai = 0;


        for (int i = 0; i < length; i++) {
            nilai += intArray[i];
        }
        System.out.print((nilai/length) + " ");
    }
}
