package InputPrural;

import java.util.Scanner;

public class Soal4Modus {
    public static void resolve(){
        Scanner input = new Scanner(System.in);

        System.out.print("input deret angka = ");
        String text = input.nextLine();

        int[] angkaArray = Utility.ConvertStringToArrayInt(text);

        int jumlahAngka = 0, jumlahmodus = 0, modus = 0;
        for (int i = 0; i < angkaArray.length; i++) {
            for (int j = 0; j < angkaArray.length; j++) {
                if (angkaArray[i] == angkaArray[j] && i != j) {
                    jumlahAngka ++;
                }
            }
            if (jumlahAngka >= jumlahmodus) {
                jumlahmodus = jumlahAngka;
                modus = angkaArray[i];
                jumlahAngka = 0;
            }
        }
        System.out.println(modus);
    }
}
