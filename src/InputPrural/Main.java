package InputPrural;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0 ;
        boolean flag = true;

        while (flag){
            System.out.println("soal nomer 1 - 12");
            System.out.print("nomor = ");
            pilihan = input.nextInt();

            while (pilihan < 1 || pilihan > 12){
                System.out.println("angka tidak tersedia");
                pilihan = input.nextInt();
            }


            switch (pilihan){
                case 1 :
                    Soal1JumlahDeret.resolve();
                    break;
                case 2:
                    Soal2RataRata.resolve();
                    break;
                case 3:
                    Soal3Median.resolve(); // ini pakek sort
                    break;
                case 4:
                    Soal4Modus.resolve(); // belum
                    break;
                case 5:
                    Soal5Sorting.resolve();
                    break;
                case 6:
                    Soal6.resolve();
                    break;
                case 7:
                    Soal7nilaiterbesardanterkecil.resolve(); // ini pakek sort
                    break;
                case 8:
                    Soal8banyakAngkaterbesar.resolve();
                    break;
                case 9:
                    Soal9positivenegatifzero.resolve();
                    break;
                case 10:
                    Soal10munculinprima.resolve();
                    break;
                case 11:
                    Soal11ngurutinASCdanDSC.resolve();
                    break;
                case 12:
                    Soal12peringkatPlayer.Hasil();
                   break;

            }
        }
    }
}
