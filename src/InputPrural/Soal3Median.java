package InputPrural;

import java.util.Arrays;
import java.util.Scanner;

public class Soal3Median {
    public static void resolve(){
        Scanner input = new Scanner(System.in);

        System.out.print("input deret angka = ");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;
        Arrays.sort(intArray);

        for (int i = 0; i < length; i++) {
            if (i == length / 2){
                System.out.print(intArray[i] + " ");
            } else if (i == length/2 - 1) {
                System.out.print(intArray[i] + " ");
            }
        }
        System.out.println();
    }
}
