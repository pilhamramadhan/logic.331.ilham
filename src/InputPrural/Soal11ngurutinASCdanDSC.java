package InputPrural;

import java.util.Scanner;

public class Soal11ngurutinASCdanDSC {
    public static void resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("masukan data yang ingin di urutkan = ");
        String huruf = input.nextLine();
        System.out.println("pilih ASC atau DSC = ");
        String urut = input.nextLine().toLowerCase();

        char[] hurufPisah = huruf.toLowerCase().toCharArray();
        char helper;

        if (urut.equals("asc")){
            for (int i = 0; i < hurufPisah.length - 1 ; i++) {
                for (int j = i + 1; j < hurufPisah.length; j++) {
                    if (hurufPisah[i] > hurufPisah[j]){
                        helper = hurufPisah[i];
                        hurufPisah[i] = hurufPisah[j];
                        hurufPisah[j] = helper;
                    }
                }
            }
            System.out.println("hasil dari ascending =");
            System.out.println(hurufPisah);
        }
        else if (urut.equals("dsc")){
            for (int i = 0; i < hurufPisah.length - 1 ; i++) {
                for (int j = i + 1; j < hurufPisah.length; j++) {
                    if (hurufPisah[i] < hurufPisah[j]){
                        helper = hurufPisah[i];
                        hurufPisah[i] = hurufPisah[j];
                        hurufPisah[j] = helper;
                    }
                }
            }
            System.out.println("hasil dari descending =");
            System.out.println(hurufPisah);
        }
    }
}
