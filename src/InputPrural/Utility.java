package InputPrural;

public class Utility {
    public static int[] ConvertStringToArrayInt (String text){
        String[] textArray = text.split(" ");
        int[] intArray = new int[textArray.length];

        for (int i = 0; i < textArray.length; i++) {
            intArray[i] = Integer.parseInt(textArray[i]);
        }
        return intArray;
    }

    public static int[][] ConvertStringToArray2D (String text){
        String[] textArray = text.split(" ");
        int[][] intArray = new int[textArray.length][textArray.length];

        for (int i = 0; i < textArray.length; i++) {
            intArray[i][i] = Integer.parseInt(textArray[i]);
        }
        return intArray;
    }
}
