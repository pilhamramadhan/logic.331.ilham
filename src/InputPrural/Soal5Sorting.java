package InputPrural;

import java.util.Scanner;

public class Soal5Sorting {
    public static void resolve() {
        Scanner input  = new Scanner(System.in);

        System.out.println("masukan deret angka = ");
        String angka = input.nextLine();

        int[] angakAray = Utility.ConvertStringToArrayInt(angka);
        int helper = 0;

        for (int i = 0; i < angakAray.length ; i++) {
            for (int j = i + 1; j < angakAray.length; j++) {
               if ( angakAray[i] > angakAray[j]){
                   helper = angakAray[i];
                   angakAray[i] = angakAray[j];
                   angakAray[j] = helper;
               }
            }
        }
        for (int i = 0; i < angakAray.length; i++) {
            System.out.print(angakAray[i]+ " ");
        }
        System.out.println("");
    }
}

