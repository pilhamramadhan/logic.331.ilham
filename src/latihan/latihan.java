package latihan;

import javax.swing.plaf.synth.SynthTextAreaUI;
import java.util.Scanner;

public class latihan {
    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        boolean flag = true;

        while (flag){
            System.out.println("masukan latihan nomer berapa = ");
            int masukan = input.nextInt();

            while (masukan < 1 || masukan > 10){
                System.out.println("pilihan tidak ada");
                masukan = input.nextInt();
            }
            switch (masukan){
                case 1:
                    Latihan1();
                    break;
                case 2 :
                    Latihan2();
                    break;
                case 3:
                    Latihan3();
                    break;
                case 4:
                    Latihan4();
                    break;
                case 5:
                    Latihan5();
                    break;
            }
        }

    }

    public static void Latihan1(){
        System.out.print("masukan nilai n = ");
        int n = input.nextInt();

        int[] nilai = new int[n];
        int helper = 1;

        for (int i = 0; i < n; i++) {
            nilai[i] = helper;
            helper++;
            System.out.print(nilai[i]+ " ");
        }
    }

    public static void Latihan2(){
        //6,20,42,72,110,
        // 14, 22, 30, 38
        // 8 8 8 8
        System.out.println("masukan nilai n = ");
        int n = input.nextInt();
        int helper = 6;
        int helper2 = 14;
        int helper3 = 8;

        int[] nilai = new int[n];
        for (int i = 0; i < n; i++) {
            nilai[i] = helper;
            helper += helper2;
            helper2 += helper3;
            System.out.print(nilai[i]+ " ");
        }
    }

    public static void Latihan3(){
        System.out.println("masukan nilai n = ");
        int n = input.nextInt();

        int helper = 6;
        int helper2 = 14;
        int helper3 = 8;

        int[] nilai = new int[n];

        for (int i = 0; i < n; i++) {
            nilai[i] = helper;
            helper += helper2;
            helper2 += helper3;
        }
        for (int i = 0; i < nilai.length; i++) {
            if (nilai[i] % 4 == 0){
                System.out.print("* ");
            } else {
                System.out.print(nilai[i] + " ");
            }
        }
    }

    public static void Latihan4(){
        System.out.println("masukan jumlah n = ");
        int n = input.nextInt();

        int[] deret = new int[n];
        int helper = 1;
        boolean flag = true;

        for (int i = 0; i < n; i++) {
            while (flag){
                if (helper == 1){
                    helper++;
                } else if (helper == 2) {
                    deret[i] = helper;
                    helper++;
                    flag = false;
                } else if (helper == 3) {
                    deret[i] = helper;
                    helper++;
                    flag = false;
                } else if (helper % 2 != 0 && helper % 3 != 0) {
                    deret[i] = helper;
                    helper++;
                    flag = false;
                } else {
                    helper++;
                }
            }
            flag = true;
            System.out.print(deret[i] + " ");
        }
    }

    public static void Latihan5(){
        System.out.println("masukan niali n =");
        int n = input.nextInt();

        int[][] hasil = new int[2][n];
        int helper = 1;
        int helper2 = n*1;

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n ; j++) {
                if (i == 0){
                    hasil[i][j]= helper;
                    helper++;
                } else if (i == 1) {
                    hasil[i][j] = helper2;
                    helper2 --;
                }
                System.out.print(hasil[i][j] + " ");
            }
            System.out.println(" ");
        }
    }
}


