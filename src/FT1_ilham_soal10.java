import java.util.Scanner;

public class FT1_ilham_soal10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("masukan kalimat = ");
        String kalimat = input.nextLine();
        char[] kalimatArray = kalimat.toCharArray();

        String kosonan = "bcdfghjklmnpqrstvwxyz";
        char[] kosonanArray = kosonan.toCharArray();

        String vokal = "aiueo";
        char[] vokalArray = vokal.toCharArray();

        int count = 0;
        for (int i = 0; i < kalimatArray.length; i++) {
            for (int j = 0; j < kosonanArray.length; j++) {
                    if (kalimatArray[i] == kosonanArray[j]) {
                        count ++;
                    }
            }
        }

        System.out.print(count);

    }
}
