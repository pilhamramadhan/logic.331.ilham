package pretest;

import java.util.Scanner;

public class Soal8PrimadanFibonanci {
    public static void Hasil(){
        Scanner input = new Scanner(System.in);

        System.out.println("input kan panjang n =");
        int n = input.nextInt();

        int[] panjangPrima = new int[n];
        int[] panjangFibonanci = new int[n];
        int[] panjangDariJumlahDeret = new int[n];

        boolean flag = true;
        int helper1 = 1;

        int helper2 = 0;
        int helper3 = 1;

            for (int i = 0; i < n; i++) {
                while (flag){
                    if (helper1 == 1){
                        helper1++;
                    } else if (helper1 == 2) {
                        panjangPrima[i] = helper1;
                        helper1++;
                        flag = false;
                    } else if (helper1 == 3) {
                        panjangPrima[i] = helper1;
                        helper1++;
                        flag = false;
                    } else if (helper1 % 2 != 0 && helper1 % 3 != 0) {
                        panjangPrima[i] = helper1;
                        helper1 ++;
                        flag = false;
                    } else {
                        helper1++;
                    }
                }
                flag = true;
                System.out.print(panjangPrima[i] + " ");
            }

        System.out.println();
        for (int i = 0; i < n; i++) {
            panjangFibonanci[i]= helper2 + helper3;
            helper2 = helper3;
            helper3 = panjangFibonanci[i];

            System.out.print(panjangFibonanci[i] + " ");
        }

        System.out.println();
        System.out.println("hasil dari penjumlahan di atas = ");
        for (int i = 0; i < n; i++) {
            panjangDariJumlahDeret[i] = panjangFibonanci[i] + panjangPrima[i];
            System.out.print(panjangDariJumlahDeret[i]+ " ");
        }
        System.out.println();
    }
}
