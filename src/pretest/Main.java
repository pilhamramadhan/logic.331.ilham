package pretest;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
            boolean flag = true;

            while (flag){
                System.out.println("masukan pretes nomer berapa = ");
                int masukan = input.nextInt();

                while (masukan < 1 || masukan > 10){
                    System.out.println("pilihan tidak ada");
                    masukan = input.nextInt();
                }
                switch (masukan){
                    case 1:
                        Soal1.Hasil();
                        break;
                    case 2:
                        Soal2VokaldanKosonan.Hasil();
                        break;
                    case 3:
                        Soal3Siangka1.Hasil();
                        break;
                    case 4:
                        Soal4Toko.Hasil();
                        break;
                    case 5:
                        Soal5banyakPorsi.Hasil();
                        break;
                    case 6:
                        Soal6bank.Hasil();
                        break;
                    case 7:
                        Soal7judi.Hasil();
                        break;
                    case 8:
                        Soal8PrimadanFibonanci.Hasil();
                        break;
                    case 9:
                        Soal9Ninjahatori.Hasil();
                        break;
                    case 10:
                        Soal10diskon.Hasil();
                        break;

                }
            }

        }
    }

