package pretest;

import java.util.Arrays;
import java.util.Scanner;

public class Soal2VokaldanKosonan {
    public static void Hasil(){
        Scanner input = new Scanner(System.in);

        System.out.println("masukan kalimat = ");
        String kalimat = input.nextLine();
        char[] kalimatArray = kalimat.toCharArray();

        String kosonan = "bcdfghjklmnpqrstvwxyz";
        char[] kosonanArray = kosonan.toCharArray();

        String vokal = "aiueo";
        char[] vokalArray = vokal.toCharArray();

        Arrays.sort(kalimatArray);
        for (int i = 0; i < kalimatArray.length; i++) {
            for (int j = 0; j < kosonanArray.length; j++) {
                if (kalimatArray[i] == kosonanArray[j]) {
                    System.out.print(kalimatArray[i]);
                }
            }
        }
        System.out.println();
        for (int i = 0; i < kalimatArray.length; i++) {
            for (int j = 0; j < vokalArray.length; j++) {
                if (kalimatArray[i] == vokalArray[j]){
                    System.out.print(kalimatArray[i]);
                }
            }
        }
        System.out.println();
    }
}
