package WarmUp;

import java.util.Scanner;

public class Staircase {
    public static void HasilArray() {
        Scanner input = new Scanner(System.in);

        System.out.print("input n = ");
        int n = input.nextInt();

        int[][] hasil = new int[n][n];


        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if ((j + i) >= n - 1) {
                    hasil[i][j] = 0;
                } else {
                    hasil[i][j] = 1;
                }
            }
        }
        for (int i = 0; i < hasil.length; i++) {
            for (int j = 0; j < hasil[0].length; j++) {
                if (hasil[i][j] == 0) {
                    System.out.print("# ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println(" "); // memberi line
        }
    }
}
