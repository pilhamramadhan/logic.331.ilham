package WarmUp;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        boolean flag = true;

        while (flag){
            System.out.println("soal nomer 1 - 10");
            System.out.print("nomor = ");
            pilihan = input.nextInt();

            while (pilihan < 1 || pilihan > 12) {
                System.out.println("soal tidak tersedia");
                pilihan = input.nextInt();
            }


            switch (pilihan) {
                case 1:
                    SolveMeFirst.Hasil();
                    break;
                case 2:
                    TimeConversion.Hasil();
                    break;
                case 3:
                    SimpleArraySum.Hasil();
                    break;
                case 4:
                    DiagonalDifference.Hasil();
                    break;
                case 5:
                    PlusMinus.Hasil();
                    break;
                case 6:
                    Staircase.HasilArray();
                    break;
                case 7:
                    MiniMaxSum.Hasil();
                    break;
                case 8:
                    BirthdayCakeCandles.Hasil(); // belum
                    break;
                case 9:
                    AVeryBigSum.Hasil();
                    break;
                case 10:
                    ComparetheTriplets.Hasil();
                    break;
            }
        }
    }
}