package WarmUp;

import java.util.Scanner;

public class TimeConversion {
    public static void Hasil(){
//        Scanner input = new Scanner(System.in);
//
//        System.out.print("masukan jam = ");
//        int masukanJam = input.nextInt();
//        System.out.print("masukan menit = ");
//        int masukanMenit = input.nextInt();
//        System.out.print("masukan detik = ");
//        int masukanDetik = input.nextInt();
//
//
//        if (masukanJam < 12 ){
//            masukanJam += 12;
//            if (masukanMenit < 12){
//                masukanMenit += 0;
//                masukanDetik -= 0;
//            }
//        } else if (masukanJam > 12 ){
//            masukanJam += 0;
//            if (masukanMenit < 12 ){
//                masukanMenit += 0;
//                masukanDetik -= 0;
//            }
//        }
//        System.out.println("jam = " + masukanJam +":"+masukanMenit+":"+masukanDetik);
//

        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan Jam : ");
        String inputTime = input.nextLine();


        int jam = Integer.parseInt(inputTime.substring(0, 2));
        String menit = inputTime.substring(3, 5);
        String detik = inputTime.substring(6, 8);
        String period = inputTime.substring(8, 10);

        if (period.equalsIgnoreCase("am")) {
            if (jam == 12) {
                jam = 0;
            }
        } else {
            if (jam != 12) {
                jam += 12;
            }
            System.out.println(jam + ":" + menit + ":" + detik);
        }
    }
}
