package WarmUp;

import InputPrural.Utility;

import java.util.Scanner;

public class DiagonalDifference {
    public static void Hasil(){
        Scanner input = new Scanner(System.in);

        System.out.print("masukan baris pertama = ");
        String text = input.nextLine();
        int[] intArray = Utility.ConvertStringToArrayInt(text);
        System.out.print("masukan baris kedua = ");
        String text2 = input.nextLine();
        int[] intArray2 = Utility.ConvertStringToArrayInt(text2);
        System.out.print("masukan baris ketiga = ");
        String text3 = input.nextLine();
        int[] intArray3 = Utility.ConvertStringToArrayInt(text3);

        int diagonal1 = intArray[0] + intArray2[1] + intArray3[2];
        int diagonal2 = intArray[2] + intArray2[1] + intArray3[0];

        System.out.println("hasil dari pengurangan diagonal = " + (diagonal1 - diagonal2) );
    }
}
