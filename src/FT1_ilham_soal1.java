import java.util.Scanner;

public class FT1_ilham_soal1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("silahkan masukan deret = ");
        String masukan = input.nextLine();
        String[] masukanArray = masukan.split(" ");
        double[] doubleMasukan = new double[masukanArray.length];
        for (int i = 0; i < masukanArray.length; i++) {
            doubleMasukan[i] = Double.parseDouble(masukanArray[i]);
        }

        double helper = 0.0 ;

        for (int i = 0; i < doubleMasukan.length; i++) {
            helper += doubleMasukan[i];
        }

        System.out.println(helper);

    }

}
