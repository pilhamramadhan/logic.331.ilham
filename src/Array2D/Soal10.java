package Array2D;

import java.util.Scanner;

public class Soal10 {
    public static void HasilArray() {
        Scanner input = new Scanner(System.in);

        System.out.print("input n = ");
        int n = input.nextInt();

        int[][] hasil = new int[3][n];
        int nilai = 0;
        int nilai1 = 0;
        int nilai2 = 0;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0){
                    hasil[i][j] = nilai;
                    nilai ++;
                } else if (i == 1) {
                    hasil[i][j] = nilai1;
                    nilai1 += 3;
                } else {
                    hasil[i][j] = nilai2;
                    nilai2 += 4;
                }
            }
        }
        UtilityA2D.PrintArray(hasil);
    }
}
