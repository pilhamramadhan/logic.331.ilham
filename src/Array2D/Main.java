package Array2D;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0 ;
        boolean flag = true;

        while (flag){
            System.out.println("soal nomer 1 - 12");
            System.out.print("nomor = ");
            pilihan = input.nextInt();

            while (pilihan < 1 || pilihan > 12){
                System.out.println("angka tidak tersedia");
                pilihan = input.nextInt();
            }


            switch (pilihan){
                case 1 :
                    Soal1.HasilArray();
                    break;
                case 2:
                    Soal2.HasilArray();
                    break;
                case 3:
                    Soal3.HasilArray();
                    break;
                case 4:
                    Soal4.HasilArray();
                    break;
                case 5:
                    Soal5.HasilArray();
                    break;
                case 6:
                    Soal6.HasilArray();
                    break;
                case 7:
                    Soal7.HasilArray();
                    break;
                case 8:
                    Soal8.HasilArray();
                    break;
                case 9:
                    Soal9.HasilArray();
                    break;
                case 10:
                    Soal10.HasilArray();
                    break;
                case 11:
                    Soal11.HasilArray();
                    break;
                case 12:
                    Soal12.HasilArray(); // pr
                    break;

            }

        }


    }
}