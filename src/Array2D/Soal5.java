package Array2D;

import java.util.Scanner;

public class Soal5 {
    public static void HasilArray() {
        Scanner input = new Scanner(System.in);

        System.out.print("input n = ");
        int n = input.nextInt();

        int[][] hasil = new int[3][n];
        int nilai = 0;
        int nilai1 = nilai + n;
        int nilai2 = nilai1 + n;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0){
                    hasil[i][j] = nilai;
                    nilai++;
                } else if (i == 1) {
                    hasil[i][j] = nilai1;
                    nilai1++;
                } else {
                    hasil[i][j] = nilai2;
                    nilai2++;
                }
            }
        }
        UtilityA2D.PrintArray(hasil);
    }
}
