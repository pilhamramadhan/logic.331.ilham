package Array2D;

import java.util.Scanner;

public class Soal1 {
    public static void HasilArray(){
        Scanner input = new Scanner(System.in);

        System.out.print("input n = ");
        int n = input.nextInt();

        int[][] hasil = new int[2][n];
        int nilai = 0;
        int nilai2 = 1;

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 7; j++) {
                if (i == 0){
                    hasil[i][j] = nilai;
                    nilai++;
                } else if (i == 1) {
                    hasil[i][j] = nilai2;
                    nilai2 *= 3;
                }
            }
        }
        UtilityA2D.PrintArray(hasil);
    }
}
