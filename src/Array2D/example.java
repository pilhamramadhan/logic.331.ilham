package Array2D;

import java.util.Scanner;

public class example {
    public static void HasilArray() {
        Scanner input = new Scanner(System.in);

        System.out.print("input n = ");
        int n = input.nextInt();

        int[][] hasil = new int[2][n];
        int ganjil = 1;
        int genap = 2;

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0){
                    hasil[i][j] = ganjil;
                    ganjil += 2;
                } else if (i == 1) {
                    hasil[i][j] = genap;
                    genap += 2;
                }
            }
        }
        UtilityA2D.PrintArray(hasil);
    }
}
