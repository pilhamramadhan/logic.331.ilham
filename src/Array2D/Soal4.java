package Array2D;

import java.util.Scanner;

public class Soal4 {
    public static void HasilArray() {
        Scanner input = new Scanner(System.in);

        System.out.print("input n = ");
        int n = input.nextInt();

        int[][] hasil = new int[2][n];
        int nilai = 0;
        int nilai1 = 1;
        int nilai2 = 5;

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0){
                    hasil[i][j] = nilai;
                    nilai++;
                } else if ( i == 1 ){
                    if((j+1) % 2 == 1 ){
                        hasil[i][j] = nilai1;
                        nilai1 ++;
                    }else {
                        hasil[i][j] = nilai2;
                        nilai2 += 5;
                    }
                }
            }

        }
        UtilityA2D.PrintArray(hasil);
    }
}
