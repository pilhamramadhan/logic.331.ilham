package Array2D;

import java.util.Scanner;

public class Soal6 {
    public static void HasilArray(){
        Scanner input = new Scanner(System.in);

        System.out.print("input n = ");
        int n = input.nextInt();

        int[][] hasil = new int[3][n];
        int nilai = 0;
        int nilai2 = 1;
        int nilai3 = 0;
        int nilai4 = 1;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n ; j++) {
                if (i == 0 ) {
                    hasil[i][j] = nilai;
                    nilai++;
                } else if (i==1) {
                    hasil[i][j] = nilai2;
                    nilai2 *= n;
                } else {
                    hasil[i][j] = nilai3 + nilai4;
                    nilai3 ++;
                    nilai4 *= n;
                }
            }
        }
        UtilityA2D.PrintArray(hasil);
    }
}
