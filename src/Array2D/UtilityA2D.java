package Array2D;

public class UtilityA2D {
    public static void PrintArray(int[][] hasilA){
        for (int i = 0; i < hasilA.length; i++) {
            for (int j = 0; j < hasilA[0].length; j++) {
                System.out.print(hasilA[i][j]+ " ");
            }
            System.out.println(" ");
        }
    }

    public static void PrintArrayBintang(int[][] hasil){
        for (int i = 0; i < hasil.length; i++) {
            for (int j = 0; j < hasil[0].length; j++) {
                if (hasil[i][j] == 0) {
                    System.out.print("* ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println(" "); // memberi line
            System.out.println(" ");
        }
    }
}
