import java.util.HashSet;
import java.util.Scanner;

public class FT1_ilham_soal7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("masukan kalimat = ");
        String kalimat = input.nextLine();

        String proses = kalimat.replace(",", " ");
        String proses1 = proses.replace("/", " ");
        String proses2 = proses1.replace("&", " ");
        String proses3 = proses2.replace("'", " ");
        String proses4 = proses3.replace("\"", " ");
        String proses5 = proses4.replace("@", " ");

        String[] cekKata = proses5.split(" ");
        String hasil= "";
        for (int i = 0; i < cekKata.length; i++) {
            Boolean kataSama = true;
            for (int j = 0; j < i;j++) {
                if (cekKata[i].equals(cekKata[j])){
                    kataSama = false;
                    break;
                }
            }
            if (kataSama){
                hasil += cekKata[i] + " ";
            }

        }
        System.out.println(hasil);

    }
}
