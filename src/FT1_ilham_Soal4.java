import java.util.Scanner;

public class FT1_ilham_Soal4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("masukan lembar n = ");
        int lembarAsli = input.nextInt();

        System.out.println("masukan lembar x = ");
        int lembarKe = input.nextInt();

        int[] lembarAsliArray = new int[lembarAsli];

        if (lembarKe % 2 == 1){
            int helper = 1;
            for (int i = 0; i < lembarAsli; i++) {
                lembarAsliArray[i] = helper;
                helper += 2;
            }
            for (int i = 0; i < lembarAsli; i++) {
                if (lembarKe == lembarAsliArray[i]){
                    System.out.print("lembar ke = " + (i+1));
                }
            }
        }

        if (lembarKe % 2 == 0){
            int helper = 2;
            for (int i = 0; i < lembarAsli; i++) {
                lembarAsliArray[i] = helper;
                helper += 2;
            }
            for (int i = 0; i < lembarAsli; i++) {
                if (lembarKe == lembarAsliArray[i]){
                    System.out.print("lembar ke = " + (i+1));
                }
            }
        }
    }
}
